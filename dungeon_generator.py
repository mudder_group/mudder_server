import random
from models import Room, Monster


def gen_dungeon(nb_rooms):
    # placing the first room in the middle of the matrix; in the worst case, the dungeon is generated in a line
    # shape, thus having to define matrix_length below, which allows the player to go in every direction for
    # nb_rooms number of rooms
    matrix_length = (nb_rooms * 2) - 1
    rows = [None for i in range(matrix_length)]
    matrix = [list(rows) for j in range(matrix_length)]
    # generating the first room, which is the one at the center of the matrix
    # arrays start at zero, hence the - 1 at the line below
    matrix[nb_rooms - 1][nb_rooms - 1] = Room([])
    temp_nb_rooms = nb_rooms - 1  # we already generated one room
    rooms = [matrix[nb_rooms - 1][nb_rooms - 1]]
    while temp_nb_rooms > 0:
        temp_directions = gen_directions()
        while len(temp_directions) > temp_nb_rooms:
            temp_directions = gen_directions()
        # selecting a random room in the dungeon
        current_room = random.choice(rooms)
        current_room_x, current_room_y = find_room_coordinates(current_room, matrix, matrix_length)
        for direction in temp_directions:
            if direction == "N":
                if matrix[current_room_x][current_room_y - 1] is None:
                    new_room = Room([])
                    new_room.S = current_room
                    current_room.N = new_room
                    rooms.append(new_room)
                    matrix[current_room_x][current_room_y - 1] = new_room
                    temp_nb_rooms = temp_nb_rooms - 1
                else:
                    current_room.N = matrix[current_room_x][current_room_y - 1]
                    matrix[current_room_x][current_room_y - 1].S = current_room
            elif direction == "S":
                if matrix[current_room_x][current_room_y + 1] is None:
                    new_room = Room([])
                    new_room.N = current_room
                    current_room.S = new_room
                    rooms.append(new_room)
                    matrix[current_room_x][current_room_y + 1] = new_room
                    temp_nb_rooms = temp_nb_rooms - 1
                else:
                    current_room.S = matrix[current_room_x][current_room_y + 1]
                    matrix[current_room_x][current_room_y + 1].N = current_room
            elif direction == "E":
                if matrix[current_room_x + 1][current_room_y] is None:
                    new_room = Room([])
                    new_room.W = current_room
                    current_room.E = new_room
                    rooms.append(new_room)
                    matrix[current_room_x + 1][current_room_y] = new_room
                    temp_nb_rooms = temp_nb_rooms - 1
                else:
                    current_room.E = matrix[current_room_x + 1][current_room_y]
                    matrix[current_room_x + 1][current_room_y].W = current_room
            else:
                if matrix[current_room_x - 1][current_room_y] is None:
                    new_room = Room([])
                    new_room.E = current_room
                    current_room.W = new_room
                    rooms.append(new_room)
                    matrix[current_room_x - 1][current_room_y] = new_room
                    temp_nb_rooms = temp_nb_rooms - 1
                else:
                    current_room.W = matrix[current_room_x - 1][current_room_y]
                    matrix[current_room_x - 1][current_room_y].E = current_room
    rooms_list = rooms
    print_all_rooms(rooms_list)
    return rooms_list


def print_all_rooms(rooms_list):
    count = 0
    for room in rooms_list:
        print("Pièce " + str(count) +
              " de nom " + str(room) +
              "; direction Est: " + str(room.E) +
              "; direction Ouest: " + str(room.W) +
              "; direction Nord: " + str(room.N) +
              "; direction Sud: " + str(room.S))
        count = count + 1


def find_room_coordinates(room, matrix, width):
    for x in range(width):
        for y in range(width):
            if matrix[x][y] == room:
                return x, y
    return None


def gen_directions():
    directions = ["N", "S", "E", "W"]
    result = []
    nb_rooms = random.randint(1, 4)
    for i in range(nb_rooms):
        choice = random.choice(directions)
        result.append(choice)
        directions.remove(choice)
    return result


def contains_monsters(room):
    for entity in room.entities:
        if entity.__class__ == Monster:
            return True
    return False


def gen_monsters(room):
    if not contains_monsters(room) and room.is_cleared is False:
        count = 1
        room.is_cleared = True
        nb_monsters = random.randint(0, 3)
        for i in range(nb_monsters):
            health = random.randint(10, 50)
            room.entities.append(Monster(random.randint(0, 20), f"Monstre du donjon {count}", health, health, room))
            count += 1
