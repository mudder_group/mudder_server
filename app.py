from uuid import uuid4
from flask import Flask, abort, request
import random

from models import Player, Monster
from dungeon_generator import gen_dungeon, gen_monsters

app = Flask(__name__)

uuid = uuid4().int
rooms_list = []
players_list = []
nb_rooms = 10


class DeadException(Exception):
    pass


class WallException(Exception):
    pass


class NotSameRoomException(Exception):
    pass


class PlayerNotFoundException(Exception):
    pass


class InvalidMovementException(Exception):
    pass


class EndGameException(Exception):
    pass


def find_player_by_guid(guid):
    global players_list
    for player in players_list:
        if player.guid == int(guid):
            return player
    return None


def find_opponent_by_guid(room, guid):
    for entity in room.entities:
        if entity.guid == int(guid):
            return entity
    return None


def get_entities_guids(room):
    result = []
    for entity in room.entities:
        result.append(int(entity.guid))
    return result


def is_dungeon_cleared():
    for room in rooms_list:
        if not room.is_cleared:
            return False
    return True


def update_room(room):
    contains_monsters = False
    for entity in room.entities:
        if entity.__class__ == Monster:
            contains_monsters = True
            break
    room.is_cleared = not contains_monsters


def exists(guid):
    for room in rooms_list:
        for entity in room.entities:
            if entity.guid == int(guid):
                return True
    return False


def sort_by_name(names_list, guids_list):
    temp = []
    temp_names = []
    temp_guids = []
    for index, value in enumerate(names_list):
        temp.append([value, guids_list[index]])
    names_list.sort()
    for name in names_list:
        for temp_name, guid in temp:
            if temp_name == name:
                temp_names.append(temp_name)
                temp_guids.append(guid)
                break
    return temp_names, temp_guids


def get_names(entities_list, player):
    result_names_list = []
    result_guids_list = []
    for entity in entities_list:
        if player.guid != entity.guid:
            if entity in players_list:
                result_names_list.append(entity.username)
                result_guids_list.append(entity.guid)
            else:
                result_names_list.append(entity.name)
                result_guids_list.append(entity.guid)
    result_names_list, result_guids_list = sort_by_name(result_names_list, result_guids_list)
    return result_names_list, result_guids_list


@app.route("/<guid>/noms")
def recuperer_noms(guid):
    int_guid = int(guid)
    player = find_player_by_guid(int_guid)
    room = player.room
    result_names, result_guids = get_names(room.entities, player)
    return {
        "listenoms": result_names,
        "listeguids": result_guids
    }


@app.route("/")
def welcom():
    return "Bienvenue sur Mudder ! Cette version du jeu a été réalisée par Lilian Delouvy !"


@app.route("/connect", methods=["POST"])
def connect():
    global rooms_list
    if not rooms_list:
        rooms_list = gen_dungeon(nb_rooms)
    data = request.get_json()
    room = random.choice(rooms_list)
    gen_monsters(room)
    player = Player(data["Username"], 100, 100, room)
    players_list.append(player)
    room.entities.append(player)
    directions = room.get_directions()
    result = {
        "guid": int(player.guid),
        "vie": 100,
        "totalvie": 100,
        "salle": {
            "description":
                f"Vous êtes dans la première salle du donjon. Vous pouvez aller dans {len(directions)} directions.",
            "passages": directions,
            "entites": get_entities_guids(room),
        },
    }
    print(player.guid)
    print(directions)
    return result


@app.route("/<guid>/regarder")
def regarder(guid):
    int_guid = int(guid)
    room = find_player_by_guid(int_guid).room
    directions = room.get_directions()
    description = f"Vous êtes dans une salle depuis laquelle vous pouvez aller dans {len(directions)} directions." \
                  f"Ces directions sont les suivantes : {directions}."
    print(directions)
    return {
        "description": description,
        "passages": directions,
        "entites": get_entities_guids(room),
    }


@app.route("/<guid>/deplacement", methods=["POST"])
def deplacer(guid):
    direction = request.json["direction"]
    print(f"L'utilisateur {guid} veut se déplacer vers {direction}")
    player = find_player_by_guid(int(guid))
    if player is not None:
        player.move(direction)
        gen_monsters(player.room)
        directions = player.room.get_directions()
        return {
            "description": f"Vous êtes dans une nouvelle pièce. Vous pouvez aller dans {len(directions)} directions. "
                           f"Ces directions sont les suivantes: {directions}",
            "passages": directions,
            "entites": get_entities_guids(player.room),
        }
    return error_entitynotfound()


@app.route("/<guid>/examiner/<guiddest>")
def examiner(guid, guiddest):
    if exists(guiddest):
        other = find_opponent_by_guid(find_player_by_guid(guid).room, guiddest)
        if other is None:
            return error_notsameroom()
        if other in players_list:
            is_a_player = True
        else:
            is_a_player = False

        return {
            "description": f"Il s'agit d'un {'joueur' if is_a_player else 'monstre'}.",
            "type": "Joueur" if is_a_player else "Monstre",
            "vie": other.health,
            "totalvie": other.total_health,
        }
    abort(404)


@app.route("/<guid>/taper", methods=["POST"])
def taper(guid):
    target = int(request.json["cible"])
    print(f"L'utilisateur {guid} veut attaquer la cible {target}")
    player = find_player_by_guid(guid)
    room = player.room
    if not exists(target):
        abort(404)
        return
    opponent = find_opponent_by_guid(room, target)
    if opponent is not None:
        opponent.health -= 20
        if opponent.health <= 0:
            room.entities.remove(opponent)
            update_room(room)
            if is_dungeon_cleared():
                return error_endgame()
        elif opponent.__class__ == Monster:
            player.health -= 10
            if player.health <= 0:
                players_list.remove(player)
                return error_dead()
        return {
            "attaquant": {
                "guid": guid,
                "degats": 20,
                "vie": player.health
            },
            "attaque": {
                "guid": opponent.guid,
                "degats": 10,
                "vie": opponent.health,
            },
        }
    return error_entitynotfound()


@app.errorhandler(DeadException)
def error_dead():
    return {"type": "Mort", "message": "Vous êtes mort (pas de bol)"}, 409


@app.errorhandler(NotSameRoomException)
def error_notsameroom():
    return {f"type": "Salle différente", "message": "Vous n'êtes pas dans la même salle ! "}, 409
    # "Erreur": json.dumps(error.__dict__)},\


@app.errorhandler(WallException)
def error_wall():
    return {"type": "Mur", "message": "Vous avez pris un mur"}, 409


@app.errorhandler(PlayerNotFoundException)
def error_entitynotfound():
    return {"type": "Entité introuvable", "message": "L'entité est introuvable"}, 409


@app.errorhandler(InvalidMovementException)
def error_invalidmovement():
    return {"type": "Mouvement invalide", "message": "Le mouvement souhaité est impossible"}, 409


@app.errorhandler(EndGameException)
def error_endgame():
    return {"type": "Fin de la partie", "message": "Tous les monstres du donjon ont été vaincus !"}, 409


app.run(host='0.0.0.0', port=8080)

if __name__ == '__main__':
    app.run()
