from uuid import uuid4


class Room:
    def __init__(self, entities):
        self.N = None
        self.E = None
        self.W = None
        self.S = None
        self.entities = entities
        self.is_cleared = False

    def get_directions(self):
        result = []
        if self.E is not None:
            result.append("E")
        if self.N is not None:
            result.append("N")
        if self.S is not None:
            result.append("S")
        if self.W is not None:
            result.append("W")
        return result


class Entity:
    def __init__(self, health, total_health, room):
        self.guid = uuid4().int
        self.health = health
        self.total_health = total_health
        self.room = room


class Player(Entity):
    def __init__(self, username, health, total_health, room):
        Entity.__init__(self, health, total_health, room)
        self.username = username

    def move(self, direction):
        if direction == "N":
            if self.room.N is not None:
                self.room.entities.remove(self)
                self.room.N.entities.append(self)
                self.room = self.room.N
        elif direction == "S":
            if self.room.S is not None:
                self.room.entities.remove(self)
                self.room.S.entities.append(self)
                self.room = self.room.S
        elif direction == "E":
            if self.room.E is not None:
                self.room.entities.remove(self)
                self.room.E.entities.append(self)
                self.room = self.room.E
        else:
            if self.room.W is not None:
                self.room.entities.remove(self)
                self.room.W.entities.append(self)
                self.room = self.room.W


class Monster(Entity):
    def __init__(self, attack, name, health, total_health, room):
        Entity.__init__(self, health, total_health, room)
        self.name = name
        self.attack = attack
